package com.soma.os.diningphilosopher.entity;

import com.soma.os.diningphilosopher.DiningPhilosopherApplication;
import com.soma.os.diningphilosopher.common.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public class ChopStick {

    private int id;
    private Lock lock;

    private static Logger log = LogManager.getLogger(ChopStick.class);

    public ChopStick(int id) {
        this.id = id;
        this.lock = new ReentrantLock();
    }

    public boolean pickup(Philosopher philosopher, State state) throws InterruptedException {
        if (lock.tryLock(10, TimeUnit.MILLISECONDS)) {
            System.out.println(philosopher + " picked up " + state.toString() + " " + this);
            return true;
        }
        return false;
    }

    public void putDown(Philosopher philosopher, State state) {
        lock.unlock();
        System.out.println(philosopher + " picked down " + this);
    }

    @Override
    public String toString() {
        return "ChopStick " + id;
    }
}
