package com.soma.os.diningphilosopher.entity;

import com.soma.os.diningphilosopher.DiningPhilosopherApplication;
import com.soma.os.diningphilosopher.common.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public class Philosopher implements Runnable {
    private int id;
    private ChopStick leftChopStick;
    private ChopStick rightChopStick;
    private Random random;
    private int eatingCounter;
    private volatile boolean isFull = false;

    public Philosopher(int id, ChopStick leftChopStick, ChopStick rightChopStick) {
        this.id = id;
        this.leftChopStick = leftChopStick;
        this.rightChopStick = rightChopStick;
        this.random = new Random();
    }

    public int getEatingCounter() {
        return this.eatingCounter;
    }

    public void setFull(boolean full) {
        isFull = full;
    }

    @Override
    public void run() {
        try {
            while (!isFull) {
                // Philosopher is thinking
                think();

                // Philosopher is eating if both chopsticks are free
                if (leftChopStick.pickup(this, State.LEFT)) {
                    if (rightChopStick.pickup(this, State.RIGHT)) {
                        eat();
                        rightChopStick.putDown(this, State.RIGHT);
                    }
                    leftChopStick.putDown(this, State.LEFT);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void think() throws InterruptedException {
        System.out.println(this + " is thinking...");
        Thread.sleep(random.nextInt(1000) + 1);
    }

    private void eat() throws InterruptedException {
        System.out.println(this + " is eating...");
        Thread.sleep(random.nextInt(1000) + 1);
        eatingCounter++;
    }

    @Override
    public String toString() {
        return "Philosopher " + id;
    }
}
