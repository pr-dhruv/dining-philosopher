package com.soma.os.diningphilosopher;

import com.soma.os.diningphilosopher.common.Constants;
import com.soma.os.diningphilosopher.entity.ChopStick;
import com.soma.os.diningphilosopher.entity.Philosopher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.lang.invoke.ConstantCallSite;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//@SpringBootApplication
public class DiningPhilosopherApplication {

    public static void main(String[] args) throws InterruptedException {
        //SpringApplication.run(DiningPhilosopherApplication.class, args);
        ExecutorService executorService= null;
        Philosopher[] philosophers = null;
        ChopStick[] chopSticks = null;
        try {
            philosophers = new Philosopher[Constants.NUMBER_OF_PHILOSOPHER];
            chopSticks = new ChopStick[Constants.NUMBER_OF_CHOPSTICKS];

            for (int i = 0; i < Constants.NUMBER_OF_CHOPSTICKS; i++)
                chopSticks[i] = new ChopStick(i);

            executorService = Executors.newFixedThreadPool(Constants.NUMBER_OF_PHILOSOPHER);

            for (int i = 0; i < Constants.NUMBER_OF_PHILOSOPHER; i++) {
                philosophers[i] = new Philosopher(i, chopSticks[i], chopSticks[(i + 1) % Constants.NUMBER_OF_CHOPSTICKS]);
                executorService.submit(philosophers[i]);
            }

            Thread.sleep(Constants.SIMULATION_RUNNING_TIME);

            for (Philosopher p : philosophers) {
                p.setFull(true);
            }

        } finally {
            executorService.shutdown();
            while(!executorService.isTerminated()) {
                Thread.sleep(1000);
            }
            for (Philosopher p : philosophers) {
                System.out.println(p + " eat " + p.getEatingCounter() + "times");
            }

        }
    }

}
