package com.soma.os.diningphilosopher.common;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */

public class Constants {

    public static final int NUMBER_OF_PHILOSOPHER = 5;
    public static final int NUMBER_OF_CHOPSTICKS = 5;
    public static final int SIMULATION_RUNNING_TIME =  60 * 1000; // for 2 Minute

}
