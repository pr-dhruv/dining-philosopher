package com.soma.os.diningphilosopher.common;

/**
 * @author Mahendra Prajapati
 * @since 20-09-2020
 */
public enum State {
    LEFT, RIGHT;
}
